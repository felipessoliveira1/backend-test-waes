Scenario:  Validate requests with invalid fields
 
Given I want to set the endpoint to <URL> and <PORT> with credentials <USER> and <PASS> and <PATH>
When I send a HTTP <METHOD> to the <PATH> with <BODY>
Then I should receive a response with <STATUSCODE>
Examples:
|URL		|PORT	|USER	|PASS	|PATH								|METHOD	|STATUSCODE	|BODY	|
|--Get Invalid path: Should fail
|localhost	|8081	|admin	|hero	|/waesheroes/api/v1/users/all/1		|GET	|403		|		|
|--Post extra field: Should receive 200 ok, but not insert the extra field
|localhost	|8081	|-		|-		|/waesheroes/api/v1/users/			|POST	|201		| {"field":"felipe", "username": "new_user5","isAdmin": false,"dateOfBirth": "1984-09-18","email": "new.user5@wearewaes.com","name": "New User 5","password": "wololo","superpower": "Kamehameha."}|
|--Put extra field: Should receive 200 ok, but not insert the extra field
|localhost	|8081	|dev	|wizard	|/waesheroes/api/v1/users/			|PUT	|200		| {"field":"felipe", "id": 2,"username": "dev","dateOfBirth": "1999-10-10","email": "zd.dev@wearewaes.com","isAdmin": false,"name": "Zuper Dooper Dev","password": "wizard","superpower": "A new power."}|
|--Delete with field mismatch: Should fail
|localhost	|8081	|tester	|maniac	|/waesheroes/api/v1/users/			|DELETE	|401		| {"id": 999,"name": "Al Skept-Cal Tester","username999": "tester","email": "as.tester@wearewaes.com","superpower": "Voltage AND Current.","dateOfBirth": "2014-07-15","isAdmin": false}|
