Scenario:  Validate requests with valid user credentials
 
Given I want to set the endpoint to <URL> and <PORT> with credentials <USER> and <PASS> and <PATH>
When I send a HTTP <METHOD> to the <PATH> with <BODY>
Then I should receive a response with <STATUSCODE>
Examples:
|URL		|PORT	|USER	|PASS	|PATH								|METHOD	|STATUSCODE	|BODY	|
|localhost	|8081	|admin	|hero	|/waesheroes/api/v1/users/all		|GET	|200		|		|
|localhost	|8081	|tester	|maniac	|/waesheroes/api/v1/users/access	|GET	|200		|		|
|localhost	|8081	|-		|-		|/waesheroes/api/v1/users/			|POST	|201		| {"username": "new_user","isAdmin": false,"dateOfBirth": "1984-09-18","email": "new.user@wearewaes.com","name": "New User","password": "wololo","superpower": "Kamehameha."}|
|localhost	|8081	|dev	|wizard	|/waesheroes/api/v1/users/			|PUT	|200		| {"id": 2,"username": "dev","dateOfBirth": "1999-10-10","email": "zd.dev@wearewaes.com","isAdmin": false,"name": "Zuper Dooper Dev","password": "wizard","superpower": "A new power."}|
|localhost	|8081	|tester	|maniac	|/waesheroes/api/v1/users/			|DELETE	|200		| {"id": 3,"name": "Al Skept-Cal Tester","username": "tester","email": "as.tester@wearewaes.com","superpower": "Voltage AND Current.","dateOfBirth": "2014-07-15","isAdmin": false}|
