package com.waes.backend.jbehave;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;

public class Steps extends JUnitStories {
	private CloseableHttpClient httpClient;
	private HttpClientContext context;
	private CloseableHttpResponse response;
	private HttpHost targetHost;
	private HttpGet httpGet;
	private HttpPost httpPost;
	private HttpPut httpPut;
	private HttpDeleteWithEntity httpDelete;
	private URIBuilder uriBuilder;
	private URI uri;
	private StringEntity paramsPost;
	private StringEntity paramsPut;
	private StringEntity paramsDelete;
	private CredentialsProvider credsProvider;
	private AuthCache authCache;
	private BasicScheme basicAuth;
	
    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(),
                new GivenSteps());
    }

    @Override
    public Configuration configuration() {
        return new MostUsefulConfiguration()
                .useStoryLoader(new LoadFromClasspath(this.getClass()))
                .useStoryReporterBuilder(new StoryReporterBuilder().withFormats(Format.CONSOLE, Format.HTML));
    }

    protected List<String> storyPaths() {
        return Arrays.asList("validCredentials.story","invalidField.story");
    }

    public HttpHost getHttpHost() {
    	return targetHost;
    }
    public HttpGet getHttpClientGet() {
    	return httpGet;
    }
    public HttpPost getHttpClientPost() {
    	return httpPost;
    }
    public HttpPut getHttpClientPut() {
    	return httpPut;
    }
	public HttpDeleteWithEntity getHttpClientDelete() {
		return httpDelete;
    }
	public CloseableHttpClient getHttpClient() {
		return httpClient;
    }
	public HttpClientContext getHttpClientContext() {
		return context;
    }
	public CloseableHttpResponse getHttpResponse() {
		return response;
    }

    public void setHttpHost(String url, int port, String type) {
    	this.targetHost = new HttpHost(url, port, type);
    }
    public void setHttpClientGet(String path) {
    	this.httpGet = new HttpGet(path);
    }
    public void setHttpClientPost(String path, String body) throws IOException {
    	this.httpPost = new HttpPost(path);
        this.paramsPost = new StringEntity(body);
        this.httpPost.addHeader("content-type", "application/json");
        this.httpPost.setEntity(paramsPost);
    }
    public void setHttpClientPut(String path, String body) throws ClientProtocolException, IOException {
		this.httpPut = new HttpPut(path);
        this.paramsPut = new StringEntity(body);
        this.httpPut.addHeader("content-type", "application/json");
        this.httpPut.setEntity(paramsPut);
    }
	public void setHttpClientDelete(String path, String body) throws ClientProtocolException, IOException, URISyntaxException {
		this.uriBuilder = new URIBuilder(path);
		this.uri = uriBuilder.build();
		this.httpDelete = new HttpDeleteWithEntity(uri);
        this.paramsDelete = new StringEntity(body);
        this.httpDelete.addHeader("content-type", "application/json");
        this.httpDelete.setEntity(paramsDelete);
    }
	public void setHttpContext() {
    	this.context = new HttpClientContext();
    }
	
	public void updateContext (String user, String pass) {

		this.credsProvider = new BasicCredentialsProvider();
	  	credsProvider.setCredentials(
	  	        new AuthScope(this.targetHost.getHostName(), this.targetHost.getPort()),
	  	        new UsernamePasswordCredentials(user, pass));
	  	this.httpClient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
	  	this.authCache = new BasicAuthCache();
	  	this.basicAuth = new BasicScheme();
	  	this.authCache.put(this.targetHost, basicAuth);
	  	
	  	this.context = HttpClientContext.create();
	  	this.context.setCredentialsProvider(credsProvider);
	  	this.context.setAuthCache(authCache);
	}

	public void executeResponseGet (HttpHost targetHost, HttpGet httpGet, HttpClientContext context) throws ClientProtocolException, IOException {
		this.response = this.httpClient.execute(targetHost, httpGet, context);
	}
	public void executeResponsePost (HttpHost targetHost, HttpPost httpPost) throws ClientProtocolException, IOException {
		this.response = this.httpClient.execute(targetHost, httpPost);
	}
	public void executeResponsePut (HttpHost targetHost, HttpPut httpPut, HttpClientContext context) throws ClientProtocolException, IOException {
		this.response = this.httpClient.execute(targetHost, httpPut, context);
	}
	public void executeResponseDelete (HttpHost targetHost, HttpDeleteWithEntity httpDelete, HttpClientContext context) throws ClientProtocolException, IOException {
		this.response = this.httpClient.execute(targetHost, httpDelete, context);
	}
	public void closeSockets() throws IOException {
        this.response.close();
        this.httpClient.close();
	}
	
}