package com.waes.backend.jbehave;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.util.EntityUtils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.google.common.base.Preconditions;

public class GivenSteps extends Steps {
	
	@Given("I want to set the endpoint to $URL and $PORT with credentials $USER and $PASS and $PATH")
	public void givenSetEndpoint(@Named("URL") String url, @Named("USER") String user,
	  @Named("PASS") String pass, @Named("PORT") String port, @Named("PATH") String path) throws URISyntaxException, IOException, InterruptedException, ExecutionException {
		   	Preconditions.checkArgument(StringUtils.isNotBlank(url));
		  	Preconditions.checkArgument(StringUtils.isNotBlank(port));
		  	Preconditions.checkArgument(StringUtils.isNotBlank(user));
		  	Preconditions.checkArgument(StringUtils.isNotBlank(pass));
		  	
	  		setHttpContext();
	  		setHttpClientGet(path);
		  	setHttpHost(url, Integer.valueOf(port), "http");
		  	updateContext(user, pass);
	}
			@When("I send a HTTP $METHOD to the $PATH with $BODY")
		public void WhenSendGet(@Named("PATH") String path, @Named("METHOD") String method, 
			@Named("BODY") String body) throws URISyntaxException, IOException, InterruptedException, ExecutionException 
		{
			   	Preconditions.checkArgument(StringUtils.isNotBlank(path));
			   	Preconditions.checkArgument(StringUtils.isNotBlank(method));

		  		setHttpClientPost(path, body);
		  		setHttpClientPut(path, body);
		  		setHttpClientDelete(path, body);
		  		setHttpClientGet(path);
		  		
		    	HttpClientContext context = getHttpClientContext();
		        HttpHost targetHost = getHttpHost();
		        HttpGet httpGet = getHttpClientGet();
		        HttpPost httpPost = getHttpClientPost();
		        HttpPut httpPut = getHttpClientPut();
		        HttpDeleteWithEntity httpDelete = getHttpClientDelete();
			  	
			   	if (method.equalsIgnoreCase("get")) {
			        executeResponseGet(targetHost, httpGet, context);
			        CloseableHttpResponse response = getHttpResponse();
			        
		            System.out.println("-------------WHEN---------------------------");
		            System.out.println(response.getStatusLine());
		            System.out.println(EntityUtils.toString(response.getEntity()));
		            
				}
			   	else if (method.equalsIgnoreCase("post")) {
			        executeResponsePost(targetHost, httpPost);
			        CloseableHttpResponse response = getHttpResponse();
		        
		            System.out.println("-----------------WHEN-----------------------");
		            System.out.println(response.getStatusLine());
		            System.out.println(EntityUtils.toString(response.getEntity()));
		       
				}
			   	else if (method.equalsIgnoreCase("delete")) {
			        executeResponseDelete(targetHost, httpDelete, context);
			        CloseableHttpResponse response = getHttpResponse();
		            
			        System.out.println("----------------WHEN------------------------");
		            System.out.println(response.getStatusLine());
		            System.out.println(EntityUtils.toString(response.getEntity()));
			        
				}
			   	else if (method.equalsIgnoreCase("put")) {
			  		executeResponsePut(targetHost, httpPut, context);
			        CloseableHttpResponse response = getHttpResponse();
			        
		            System.out.println("------------WHEN----------------------------");
		            System.out.println(response.getStatusLine());
		            System.out.println(EntityUtils.toString(response.getEntity()));
				}
		}
			@Then("I should receive a response with $STATUSCODE")
			public void ThenStatusCode(@Named("STATUSCODE") String statusCode) throws URISyntaxException, IOException, InterruptedException, ExecutionException {
		    	Preconditions.checkArgument(StringUtils.isNotBlank(statusCode));
		    	
		    	CloseableHttpResponse response = getHttpResponse();
		    	System.out.println("------------THEN----------------------------");
		        System.out.println(response.getStatusLine());
			   	assertEquals(Integer.parseInt(statusCode), response.getStatusLine().getStatusCode(), "Status Code is not the same");
		        closeSockets();
			}		 	 
}