# Backend For Testers

Ready-made project for a Tester/QA assignment.  
The detailed requirements for this assignment can be found at the root of this repo (Assignment for Backend Testers.pdf).  

## Requirements

- [Java 8+](https://www.oracle.com/technetwork/java/javase/downloads/index.html) JDK must be installed
- [Maven](https://maven.apache.org/download.cgi) must be installed

## Running the application

From the project root folder type in a terminal/command prompt/console:

`mvn clean spring-boot:run`

The services run on [http://localhost:8081](http://localhost:8081) by default.

You can change the port by adding the JVM property `server.port`.

Example:

`mvn clean spring-boot:run -Dserver.port=8888`

## Examples

### Swagger UI

The application integrates live documentation and examples once running.

You can access it at [http://localhost:8081/swagger-ui.html](http://localhost:8081/swagger-ui.html)

Take into account that if you change the server port, you will need to access this URL using the changed port.

### Other examples

#### Retrieving information for one user

-  Request:
    - GET `http://localhost:8081/waesheroes/api/v1/users/details?username=dev`
-  Response body:  
```
            {
              "id": 2,
              "name": "Zuper Dooper Dev",
              "username": "dev",
              "email": "zd.dev@wearewaes.com",
              "superpower": "Debug a repellent factory storage.",
              "dateOfBirth": "1999-10-10",
              "isAdmin": false
            }
```

#### Retrieving information from all users

-  Request:
    - GET `http://localhost:8081/waesheroes/api/v1/users/all`  
    - Basic Auth: `username=admin` , `password=hero`

#### Login

-  Request:
    - GET `http://localhost:8081/waesheroes/api/v1/users/access`  
    - Basic Auth: `username=tester` , `password=maniac`

#### Sign up

-  Request:
    - POST `http://localhost:8081/waesheroes/api/v1/users`
    - Header: `Content-Type=application/json`
    - Body:
```
            {  
              "username": "new_user",  
              "isAdmin": false,  
              "dateOfBirth": "1984-09-18",  
              "email": "new.user@wearewaes.com",  
              "name": "New User",  
              "password": "wololo",
              "superpower": "Kamehameha."  
            }  
```

#### Update user

-  Request:
    - PUT `http://localhost:8081/waesheroes/api/v1/users`
    - Basic Auth: `username=dev` , `password=wizard`
    - Body:
```
            {  
              "id": 2,
              "username": "dev",
              "dateOfBirth": "1999-10-10",
              "email": "zd.dev@wearewaes.com",
              "isAdmin": false,
              "name": "Zuper Dooper Dev",
              "password": "wizard",
              "superpower": "A new power."
            }  
```

#### Delete user

-  Request:
    - DELETE `http://localhost:8081/waesheroes/api/v1/users`
    - Basic Auth: `username=tester` , `password=maniac`
    - Body:
```
            {
              "id": 3,
              "name": "Al Skept-Cal Tester",
              "username": "tester",
              "email": "as.tester@wearewaes.com",
              "superpower": "Voltage AND Current.",
              "dateOfBirth": "2014-07-15",
              "isAdmin": false
            }
```
-  Response body:
```
            User 'tester' removed from database.
```

## Context of BDD tests
### Story files
 
   User abstraction layer, used to change data of testing on a concentrate manner.

	For example: 
	  - valid credentials
	  - invalid test with extra fields
	  - invalid test with missing fields
	  - invalid test with string on int fields
	  - invalid test with number on boolean fields
	  - invalid test with empty fields

### GivenSteps java class
 
   Implementation layer, using java libraries to send data requested for user.

### Steps java class

   It concentrates the variables used for implementation layer, what allow reusing of the information across the steps.

### Run

   Maven execute the tests using jbehave plugin, what allow troubleshooting for R&D team.
 	
### Jbehave Reports
	
   Consolidate the execution on html files, showing the results on web browser.

## Running the BDD tests
### How to run the web service

	open cmd prompt on windows
	execute: "cd C:/Users\Felipe\Documents\backend-for-testers"
	execute: "mvn clean spring-boot:run -Dserver.port=8081"

### How to run the test:

	open cmd prompt on windows
	execute: "cd C:/Users\Felipe\Documents\backend-for-testers"
	execute: "mvn clean integration-test"

### How to open the report:

	open windows explorer
	open the view folder ("C:\Users\Felipe\Documents\backend-test-waes\target\jbehave\view")
	open the file reports.html on your web browser
